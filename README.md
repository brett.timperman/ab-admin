## A/B testing UI

React UI for adding A/B test rules to Consul. [Reference app](https://gitlab.com/brett.timperman/ab-reference)

Requires [A/B API](https://gitlab.com/brett.timperman/ab-api) (included in `docker-compose.yml`)

### Start
```
docker-compose up
```

UI will be running at http://localhost:80
