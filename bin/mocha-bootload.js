process.env.NODE_ENV = 'test';

require('babel-polyfill');

const ignore = require('ignore-styles');
ignore.default(['.css', '.scss', '.sass', '.stylus', '.styl', '.less', '.gif', '.png', '.jpg']);

const chai = require('chai');
chai.use(require('chai-enzyme')());
chai.use(require('sinon-chai'));
chai.should();
