import React from 'react';
import { render } from 'react-dom';
import thunkMiddleware from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';
import { load } from './actions/actions.js';
import { SplitContainer } from './containers/SplitContainer';
import axios from 'axios';

const store = createStore(
    reducers,
    compose(
      applyMiddleware(thunkMiddleware),
      window.devToolsExtension ? window.devToolsExtension() : (f) => f
    )
  );

axios
  .get('/api/splits')
  .then(({data}) => store.dispatch(load(data)))

class Main extends React.Component {
  render(){
    return (
      <Provider store={store}>
        <SplitContainer />
      </Provider>
    );
  }
}

render(<Main />, document.getElementById('main'));
