'use strict';

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const app = express();

const env = process.env.NODE_ENV || 'development'
if (env === 'development') {
  const config = require('../webpack.config.js');
  const compiler = webpack(config);

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    watchOptions: {
      aggregateTimeout: 1000,
      poll: 1000
    },
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}
else {
  app.get('/static/index.js', (req, res) => {
    res.sendFile(path.join(__dirname, '../static/index.js'));
  });
}

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, './index.html'));
});

app.listen(3000, '0.0.0.0', (err) => {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://0.0.0.0:3000');
});
