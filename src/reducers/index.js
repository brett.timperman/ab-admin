import { combineReducers } from 'redux';
import splits from './split';

export default combineReducers({
  splits
})
