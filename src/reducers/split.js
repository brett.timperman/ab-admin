import { createReducer } from 'redux-act';
import { load, save, setSplit } from '../actions/actions.js';

const initialState = {}

export default createReducer({
  [load]: (state, newState) => {
    console.log('newState', newState)
    return newState
  },
  [save.request]: (state) => {
    return state
  },
  [save.ok]: (state, data) => {
    console.log('data', data)
    return state;
  },
  [save.error]: (state, error) => {
    console.log('error', error)
    return state;
  },
  [setSplit]: (state, split) => {
    return Object.assign({}, state, split);
  }
}, initialState);
