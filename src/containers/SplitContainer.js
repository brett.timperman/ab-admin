import React from 'react';
import { connect } from 'react-redux';
import { save, setSplit } from '../actions/actions';
import Slider from '../components/Slider';
import Services from '../components/Services';
import SaveButton from '../components/SaveButton';

const totalPercentage = (values) => {
  if ( !values || Object.keys(values).length == 0 )
    return 0

  return Object.keys(values).map((k) => values[k]).reduce((a,b) => a + b);
}

const mapStateToProps = (state) => {
  if ( !state.splits || Object.keys(state.splits).length == 0 ) {
    return { state, splits: [], saveDisabled: true }
  }

  let splits = Object.keys(state.splits).map((k) => {
    if ( !state.splits[k].Spread || Object.keys(state.splits[k].Spread).length == 0 ) {
      if ( state.splits[k].Services ) {
        let serviceSplits = {}
        state.splits[k].Services.forEach((service) => {
          if (service.ServiceTags) {
            service.ServiceTags.forEach((tag) => {
              serviceSplits[tag] = 0
            })
          }
        })
        return {name: k, Spread: serviceSplits}
      }
    }
    return Object.assign({}, state.splits[k], {name: k});
  })

  let saveDisabled = !splits.map((s) => totalPercentage(s.Spread) === 100)
    .reduce((a,b) => a && b);

  return {
    state,
    splits,
    saveDisabled
  };
}

const mapDispatchToProps = { save, setSplit }

const changeFn = (split, tag, callback) => (value) => {
  let newSplit = Object.assign({}, split,
    { values: Object.assign(split.Spread, { [tag]: +value }) })
  callback({ [split.name]: newSplit })
}

const sliders = (split, callback) => {
  let percentUsed = totalPercentage(split.Spread)
  let elements = Object.keys(split.Spread).map((tag) => {
    let change = changeFn(split, tag, callback)
    return (
        <Slider key={split.name+'_'+tag} label={tag} value={+split.Spread[tag]}
          max={(100-percentUsed)+split.Spread[tag]}
          valueChanged={change} />
    );
  })
  return (
    <div>
      <div>{elements}</div>
      <Services services={split.Services} />
    </div>
  )
}

class Container extends React.Component {
  render(){
    let elements = this.props.splits.map((s) => {
      return <div key={s.name}><h1>{s.name}</h1>{sliders(s, this.props.setSplit)}</div>
    })
    return (
      <div>
        <datalist id="ticks">
          <option>0</option>
          <option>25</option>
          <option>50</option>
          <option>75</option>
          <option>100</option>
        </datalist>
        {elements}
        <br />
        <SaveButton label="Save" state={this.props.state} save={this.props.save}
          disabled={this.props.saveDisabled} />
      </div>
    );
  }
}

const { arrayOf, object, bool, func } = React.PropTypes;
Container.propTypes = {
  state: object,
  splits: arrayOf(object),
  saveDisabled: bool,
  setSplit: func,
  save: func
};

export const SplitContainer = connect(mapStateToProps, mapDispatchToProps)(Container);
