import axios from 'axios';
import { createAction } from 'redux-act';
import { createActionAsync } from 'redux-act-async';

export const load = createAction('Load');
export const setSplit = createAction('Set split');

const doSave = (state) => {
  return axios
    .post('/api/splits', state)
    .then(({data}) => data);
}

export const save = createActionAsync('Save', doSave);
