import React from 'react';

Array.prototype.concatAll = function() {
  var results = [];

  this.forEach(function(subArray) {
    subArray.forEach(function(item) {
      results.push(item);
    });
  });

  return results;
};

class Services extends React.Component {
  render(){
    let uniqueTags = [...new Set(this.props.services.map((s) => s.ServiceTags).concatAll())]
    let tags = uniqueTags.map((tag) => {
      let filtered = this.props.services.filter((s) => s.ServiceTags.findIndex((i) => i === tag) > -1)
      let listItems = filtered.map((service) => {
        return <li key={service.ServiceID}>{service.Address}:{service.ServicePort}</li>
      })
      return (
        <ul key={tag}>
          <li style={{ listStyleType: "none", fontWeight: "bold" }}>{tag}</li>
          {listItems}
        </ul>
      );
    })

    return <div>{tags}</div>
  }
}

const { arrayOf, object } = React.PropTypes;

Services.propTypes = {
  services: arrayOf(object)
}

Services.defaultProps = {
  services: []
}

export default Services
