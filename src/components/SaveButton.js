import React from 'react';

class SaveButton extends React.Component {
  save(){
    this.props.save(this.props.state.splits)
  }
  render(){
    let message = this.props.disabled ? 'Traffic across each service must total 100%': ''
    return (
      <p>
        <button onClick={this.save.bind(this)} disabled={this.props.disabled}>{this.props.label}</button>
        {message}
      </p>
    );
  }
}

const { object, string, bool, func } = React.PropTypes;

SaveButton.propTypes = {
  state: object,
  label: string,
  disabled: bool,
  save: func
}

SaveButton.defaultProps = {
  state: {},
  label: 'Save',
  disabled: false
}

export default SaveButton
