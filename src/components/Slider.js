import React from 'react';

class Slider extends React.Component {
  slide(){
    this.props.valueChanged(this.refs.input.value)
  }
  value(v){
    this.props.value=v
    this.props.valueChanged(v)
  }
  zero(){
    this.value(0)
  }
  max() {
    this.value(+this.props.max)
  }
  render(){
    let style = "{ width: " + ( 5 * this.props.max ) + " }"
    let label = this.props.label !== '' ?
      <label>{this.props.label}: <strong>{this.props.value}%</strong></label> : '';
    let maxLabel = this.props.max < 3 ? '' : this.props.max;
    return (
      <div>
        <div style={{ minWidth: 565, display: "inline-block" }}>
          <button onClick={this.zero.bind(this)}>0</button>
          <input ref="input" type="range" list="ticks"
              style={{ width: (5 * this.props.max) + 10 }}
              min="0"
              max={this.props.max}
              step={this.props.step}
              value={this.props.value}
              onChange={this.slide.bind(this)} />
          <label onClick={this.max.bind(this)}>{maxLabel}</label>
        </div>
        {label}
      </div>
    );
  }
}

const { number, string, func, oneOf } = React.PropTypes;

Slider.propTypes = {
  max: number,
  step: number,
  value: number,
  label: string,
  valueChanged: func
}

Slider.defaultProps = {
  max: 100,
  step: 1,
  value: 0,
  label: ''
}

export default Slider
