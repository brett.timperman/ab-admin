FROM node:6.2.0-slim

ENV NODE_ENV production

EXPOSE 3000

WORKDIR /ab-admin

COPY . /ab-admin

RUN npm install \
 && /ab-admin/node_modules/webpack/bin/webpack.js --output-path static

CMD [ "node", "src/index.js" ]
